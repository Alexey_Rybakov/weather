package com.example.alexey.weatherapp.model

import java.util.*
import kotlin.collections.ArrayList

class WeekForecast(val date: Date, val days: ArrayList<Forecast>)