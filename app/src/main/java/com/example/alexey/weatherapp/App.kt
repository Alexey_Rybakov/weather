package com.example.alexey.weatherapp

import android.app.Application
import com.example.alexey.weatherapp.service.ServiceProvider
import timber.log.Timber

class App: Application(){
    override fun onCreate() {
        super.onCreate()
        ServiceProvider.init(applicationContext)
        Timber.plant(Timber.DebugTree())
    }
}