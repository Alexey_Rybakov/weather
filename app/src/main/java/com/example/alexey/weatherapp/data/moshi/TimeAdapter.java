package com.example.alexey.weatherapp.data.moshi;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.util.Date;


public class TimeAdapter {

    @ToJson
    String toJson(Date date) {
        return String.valueOf(date.getTime() / 1000);
    }

    @FromJson
    Date fromJson(String date){
        return new Date(Long.valueOf(date)* 1000);
    }

}
