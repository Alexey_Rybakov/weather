package com.example.alexey.weatherapp.view

import com.example.alexey.weatherapp.model.WeatherForecast
import com.example.alexey.weatherapp.model.WeatherNow
import com.example.alexey.weatherapp.model.WeekForecast
import com.hannesdorfmann.mosby3.mvp.MvpView

interface WeatherView: MvpView{

    fun getCity(): String

    fun setData(data: List<WeekForecast>)

    fun showError(m: String)

    fun showProgress(show: Boolean)
}