package com.example.alexey.weatherapp.view


import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.example.alexey.weatherapp.R
import com.example.alexey.weatherapp.databinding.WeatherItemBinding
import com.example.alexey.weatherapp.databinding.WeatherScreenBinding
import com.example.alexey.weatherapp.model.WeekForecast
import com.hannesdorfmann.mosby3.mvp.MvpFragment

class WeatherFragment : MvpFragment<WeatherView, WeatherPresenter>(), WeatherView {

    var binding: WeatherScreenBinding? = null

    override fun showProgress(show: Boolean) {
        for (i in 0 until binding!!.rootLay.childCount){
            if (binding!!.rootLay.getChildAt(i) == binding!!.progress){
                binding!!.rootLay.getChildAt(i).visibility = if (show) View.VISIBLE else View.GONE
            }else{
                binding!!.rootLay.getChildAt(i).visibility = if (show) View.GONE else View.VISIBLE
            }
        }

    }

    override fun showError(m: String) {
        Toast.makeText(context, m, Toast.LENGTH_SHORT).show()
    }

    var adapter: WeatherAdapter? = null

    override fun setData(data: List<WeekForecast>) {
        adapter!!.setData(data)
        binding!!.city.text = getCity()
        val forecast = data[0]
        binding!!.weather.text = forecast.days[0].weather[0].description
        binding!!.temp.text = Math.round(forecast.days[0].main.temp - 273).toString()
        binding!!.textView3.text = mapIcon(forecast.days[0].weather[0].icon)
        binding!!.windValue.text = Math.round(forecast.days[0].wind.speed).toString()+"km/h"
        binding!!.humidityValue.text = Math.round(forecast.days[0].main.humidity).toString()+ " %"
    }

    override fun getCity(): String {
        return if (city == null) "Ростов" else city!!
    }

    private var city: String? = null

    fun changeCity(c: Context){
        var alert: AlertDialog? = null
        alert = AlertDialog.Builder(c)
                .setTitle("Выберите город")
                .setView(R.layout.city_choose)
                .setPositiveButton("Ок", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        city = alert!!.findViewById<EditText>(R.id.city)!!.text.toString()
                        presenter.load()
                    }
                })
                .show()
    }

    override fun createPresenter(): WeatherPresenter {
        return WeatherPresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = WeatherScreenBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recycler = view.findViewById<RecyclerView>(R.id.recycler)
        adapter = WeatherAdapter()
        recycler.adapter = adapter
        view.findViewById<View>(R.id.city).setOnClickListener {
            if (context!=null)
            changeCity(context!!)
        }
        view.findViewById<View>(R.id.imageView).setOnClickListener {
            presenter.load()
        }
    }

    class ViewHolder(val binding: WeatherItemBinding) : RecyclerView.ViewHolder(binding.root)
    }

fun mapIcon(code: String): String {
    var code = code.replace("n","d")
    return when (code) {
        "01d" -> "\uf00d"//clear sky
        "02d" -> "\uf002"//few clouds
        "03d" -> "\uf07d"//scattered clouds
        "04d" -> "\uf07d"//broken clouds
        "09d" -> "\uf004"//shower rain
        "10d" -> "\uf005"//rain
        "11d" -> "\uf005"//thunderstorm
        "13d" -> "\uf065"//snow
        "50d" -> "\uf085"//mist
        else -> "\uf085"
    }
}
