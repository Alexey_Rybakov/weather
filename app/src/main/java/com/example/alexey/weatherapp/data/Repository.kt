package com.example.alexey.weatherapp.data

import com.example.alexey.weatherapp.model.WeatherNow
import com.example.alexey.weatherapp.model.WeekForecast
import io.reactivex.Observable
import io.reactivex.Single
import java.lang.Exception

class Repository(val api: Api,val appId: String){


    fun weather(city: String): Observable<WeatherNow>{
        return api.weather(appId, city).toObservable()
    }

    fun forecast(city: String): Single<List<WeekForecast>> {
        return api.forecast(appId, city).map {
            if (it.cod!=200){
                throw Exception("")
            }else{
                return@map DataMapper.map(it)
            }
        }
    }


}