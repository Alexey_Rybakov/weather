package com.example.alexey.weatherapp.data

import com.example.alexey.weatherapp.model.WeatherForecast
import com.example.alexey.weatherapp.model.WeatherNow
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api{

    @GET("weather")
    fun weather(
            @Query("APPID") appId: String,
            @Query("q") cityName: String
    ): Single<WeatherNow>

    @GET("forecast")
    fun forecast(
            @Query("APPID") appId: String,
            @Query("q") cityName: String
    ): Single<WeatherForecast>

}