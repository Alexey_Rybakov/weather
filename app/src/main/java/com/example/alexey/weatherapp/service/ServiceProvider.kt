package com.example.alexey.weatherapp.service

import android.annotation.SuppressLint
import android.content.Context
import com.example.alexey.weatherapp.Const
import com.example.alexey.weatherapp.data.Api
import com.example.alexey.weatherapp.data.moshi.TimeAdapter
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory


class  ServiceProvider private constructor(val context: Context){

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var instance : ServiceProvider? = null

        fun init(context: Context){
            instance = ServiceProvider(context)
        }

        fun getInstance(): ServiceProvider{
            if (instance == null) throw IllegalStateException()
            return instance!!
        }

    }

    private val retrofit: Retrofit
    private val okHttpClient: OkHttpClient
    val api: Api

    init {
        okHttpClient = okHttpClient()
        retrofit = retrofit(okHttpClient, moshi())
        api = api(retrofit)
    }

    private fun moshi(): Moshi{
        return Moshi.Builder()
                .add(TimeAdapter())
                .build()
    }

    private fun okHttpClient(): OkHttpClient{
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
    }

    private fun retrofit(client: OkHttpClient, moshi: Moshi): Retrofit{
        return Retrofit.Builder()
                .client(client)
                .baseUrl(Const.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
    }

    private fun api(retrofit: Retrofit) = retrofit.create(Api::class.java)
}
