package com.example.alexey.weatherapp.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.alexey.weatherapp.databinding.WeatherItemBinding
import com.example.alexey.weatherapp.model.WeekForecast
import java.text.SimpleDateFormat

class WeatherAdapter : RecyclerView.Adapter<WeatherFragment.ViewHolder>() {

    private var inflater: LayoutInflater? = null

    private val data = ArrayList<WeekForecast>()

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        inflater = LayoutInflater.from(recyclerView.context)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        inflater = null
    }

    fun setData(data: List<WeekForecast>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherFragment.ViewHolder {
        return WeatherFragment.ViewHolder(WeatherItemBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    val format = SimpleDateFormat("EEE");

    override fun onBindViewHolder(holder: WeatherFragment.ViewHolder, position: Int) {
        val day: WeekForecast = data.get(position)
        holder.binding.day.text = format.format(day.date)
        val temp = Math.round(day.days[0].main.temp - 273)
        holder.binding.temp.text = "$temp °"
        holder.binding.weather.text = mapIcon(day.days[0].weather[0].icon)
    }
}