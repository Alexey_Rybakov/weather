package com.example.alexey.weatherapp.view

import com.example.alexey.weatherapp.Const
import com.example.alexey.weatherapp.data.Repository
import com.example.alexey.weatherapp.model.WeatherForecast
import com.example.alexey.weatherapp.plusAssign
import com.example.alexey.weatherapp.service.ServiceProvider
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.IOException

class WeatherPresenter: MvpBasePresenter<WeatherView>(){

    val repository: Repository = Repository(ServiceProvider.getInstance().api, Const.APP_KEY)

    val disposables = CompositeDisposable()

    var progress = true

    private fun syncState(){
        ifViewAttached {
            it.showProgress(progress)
        }
    }

    override fun attachView(view: WeatherView) {
        super.attachView(view)
        load()
    }

    fun load(){
        progress = true
        syncState()
        disposables += repository.forecast(view.getCity())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    result ->
                    ifViewAttached {
                        it.setData(result)
                        progress = false
                        syncState()
                    }
                }, { error ->
                    if (error is IOException){
                        ifViewAttached { it.showError("Нет подключения") }
                    }else{
                        ifViewAttached { it.showError("Ошибка") }
                    }
                    progress = false
                    syncState()
                })
    }

    override fun destroy() {
        super.destroy()
        disposables.clear()
    }

}
