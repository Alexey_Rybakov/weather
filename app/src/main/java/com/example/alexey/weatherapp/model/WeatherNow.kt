package com.example.alexey.weatherapp.model

import com.squareup.moshi.JsonClass
import java.util.*


@JsonClass(generateAdapter = true)
class WeatherNow(
        val id: String,
        val dt: Date,
        val name: String,
        val coord: Coord,
        val weather: List<Weather>,
        val wind: Wind
)
@JsonClass(generateAdapter = true)
class Coord(val lat: Double, val lon: Double)
@JsonClass(generateAdapter = true)
class Main(
       val temp: Double,
       val pressure: Double
)
@JsonClass(generateAdapter = true)
class Wind(
        val speed: Double,
        val deg: Double
)
@JsonClass(generateAdapter = true)
class Weather(
        val id: String,
        val main: String,
        val description: String,
        val icon: String
)
