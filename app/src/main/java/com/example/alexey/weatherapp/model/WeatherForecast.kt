package com.example.alexey.weatherapp.model

import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class WeatherForecast(
        val cod: Int,
        val list: List<Forecast>

)

@JsonClass(generateAdapter = true)
class Forecast(
        val dt: Date,
        val weather: List<WeatherF>,
        val wind: WindF,
        val main: MainF
)

@JsonClass(generateAdapter = true)
class WindF(
        val speed: Double,
        val deg: Double
)

@JsonClass(generateAdapter = true)
class WeatherF(
        val id: String,
        val main: String,
        val description: String,
        val icon: String
)

@JsonClass(generateAdapter = true)
class MainF(
        val temp: Double,
        val temp_min: Double,
        val temp_max: Double,
        val pressure: Double,
        val humidity: Double

)