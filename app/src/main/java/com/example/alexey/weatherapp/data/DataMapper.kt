package com.example.alexey.weatherapp.data

import com.example.alexey.weatherapp.model.Forecast
import com.example.alexey.weatherapp.model.WeatherForecast
import com.example.alexey.weatherapp.model.WeekForecast
import java.util.*

class DataMapper {

    companion object{

        fun map(data: WeatherForecast): ArrayList<WeekForecast>{

            val result = ArrayList<WeekForecast>()
            var current: WeekForecast? = null
            val day = Calendar.getInstance()

            for (forecast in data.list) {
                if (current != null){
                    val lastDay = day.time
                    day.time = forecast.dt
                    setMidnight(day)
                    if (lastDay == day.time){
                        current.days += forecast
                        continue
                    }
                }
                day.time = forecast.dt
                setMidnight(day)
                val f = ArrayList<Forecast>()
                f += forecast
                current = WeekForecast(day.time, f)
                result.add(current)
            }
            return result
        }

        private fun setMidnight(c: Calendar){
            c.set(Calendar.HOUR_OF_DAY, 0)
            c.set(Calendar.MINUTE, 0)
            c.set(Calendar.SECOND, 0)
            c.set(Calendar.MILLISECOND, 0)
        }

    }

}